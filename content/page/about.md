---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

### Interest

Health Big Data and Data Science (data processing, exploration, visualisation and analysis)

Reproducible research

Adherence to quality indicators in acute care

Prevention of loss of autonomy in elderly patients

### Professional experiences

2019 - : Assistant Professor, Lille University (ILIS), Lille, France

2018-2019 : Part-time Teacher, Lille University (ILIS), Lille France

2011 - : Data Scientist, Lille University Hospital, Lille, France

### Education

**PhD** in Clinical Research, Technological Innovation and Public Health, 2011-2015, EA2694, Ecole Doctorale Biologie-Santé, Lille University, France
Prevention of risks related to anesthesia by the valorisation of hospital information within a data warehouse

**DU** Biostatistics Applied to Medical Research and Epidemiology, 2012-2013, Lille University, France

**Master** in Computer Science, 2010-2011, Valenciennes University, France

### Publications

Visade F, Babykina G, **Lamer A**, Defebvre MM, Verloop D, Ficheur G, Genin M, Puisieux F, Beuscart JB. Importance of previous hospital stays on the risk of hospital re-admission in older adults: a real-life analysis of the PAERPA study population. Age Ageing. 2020 Jul 20:afaa139. doi: 10.1093/ageing/afaa139. Epub ahead of print. PMID: 32687169.

**Lamer A**, Laurent G, Pelayo S, El Amrani M, Chazard E, Marcilly R. Exploring Patient Path Through Sankey Diagram: A Proof of Concept. Stud Health Technol Inform. 2020 Jun 16;270:218-222. doi: 10.3233/SHTI200154. PMID: 32570378.

Laurent G, Moussa MD, Cirenei C, Tavernier B, Marcilly R, **Lamer A**. Development, implementation and preliminary evaluation of clinical dashboards in a department of anesthesia. J Clin Monit Comput. 2020 May 16:1–10. doi: 10.1007/s10877-020-00522-x. Epub ahead of print. PMID: 32418147; PMCID: PMC7229430.

Degoul S, Chazard E, **Lamer A**, Lebuffe G, Duhamel A, Tavernier B. lntraoperative administration of 6% hydroxyethyl starch 130/0.4 is not associated with acute kidney injury in elective non-cardiac surgery: A sequential and propensity-matched analysis. Anaesth Crit Care Pain Med. 2020 Apr;39(2):199-206. doi: 10.1016/j.accpm.2019.08.002. Epub 2020 Feb 14. PMID: 32068135.

**Lamer A**, Depas N, Doutreligne M, Parrot A, Verloop D, Defebvre MM, Ficheur G, Chazard E, Beuscart JB. Transforming French Electronic Health Records into the Observational Medical Outcome Partnership's Common Data Model: A Feasibility Study. Appl Clin Inform. 2020 Jan;11(1):13-22. doi: 10.1055/s-0039-3402754. Epub 2020 Jan 8. Erratum in: Appl Clin Inform. 2020 Jan;11(1):e1. PMID: 31914471; PMCID: PMC6949163.

Moussa MD, Durand A, Leroy G, Vincent L, **Lamer A**, Gantois G, Joulin O, Ait-Ouarab S, Deblauwe D, Caroline B, Decoene C, Vincentelli A, Vallet B, Labreuche J, Kipnis E, Robin E. Central venous-to-arterial PCO2 difference, arteriovenous oxygen content and outcome after adult cardiac surgery with cardiopulmonary bypass: A prospective observational study. Eur J Anaesthesiol. 2019 Apr;36(4):279-289. doi: 10.1097/EJA.0000000000000949. PMID: 30664011.

Chazard E, Ficheur G, Caron A, **Lamer A**, Labreuche J, Cuggia M, Genin M, Bouzille G, Duhamel A. Secondary Use of Healthcare Structured Data: The Challenge of Domain-Knowledge Based Extraction of Features. Stud Health Technol Inform. 2018;255:15-19. PMID: 30306898.

**Lamer A**, Ficheur G, Rousselet L, van Berleere M, Chazard E, Caron A. From Data Extraction to Analysis: Proposal of a Methodology to Optimize Hospital Data Reuse Process. Stud Health Technol Inform. 2018;247:41-45. PMID: 29677919.

**Lamer A**, Jeanne M, Ficheur G, Marcilly R. Automated Data Aggregation for Time-Series Analysis: Study Case on Anaesthesia Data Warehouse. Stud Health Technol Inform. 2016;221:102-6. PMID: 27071886.


**Lamer A**, Jeanne M, Marcilly R, Kipnis E, Schiro J, Logier R, Tavernier B. Methodology to automatically detect abnormal values of vital parameters in anesthesia time-series: Proposal for an adaptable algorithm. Comput Methods Programs Biomed. 2016 Jun;129:160-71. doi: 10.1016/j.cmpb.2016.01.004. Epub 2016 Jan 14. PMID: 26817405.

**Lamer A**, De Jonckheere J, Marcilly R, Tavernier B, Vallet B, Jeanne M, Logier R. A substitution method to improve completeness of events documentation in anesthesia records. J Clin Monit Comput. 2015 Dec;29(6):741-7. doi: 10.1007/s10877-015-9661-3. Epub 2015 Jan 30. PMID: 25634428.

### Hobbies

Painting, Wrestling